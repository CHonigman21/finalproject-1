import tkinter as tk
from PIL import ImageTk
from PIL import Image
from PIL import ImageEnhance
import ResizeRatio
import shuffle
from ConcentricRings import alternateRings

def callback(pan):
    pil_image = pan.photo

    enhancer = ImageEnhance.Contrast(pil_image)
    pil_image = enhancer.enhance(2)

    pan.photo = pil_image
    img2 = ImageTk.PhotoImage(pil_image)
    pan.configure(image=img2)
    pan.image = img2

def gui_adjust_aspect_ratio(panel):
    def helper():
        width = int (width_input.get())
        height = int (height_input.get())

        resized = ResizeRatio.resizeRatio (panel.photo.resize(panel.size), width, height)

        panel.size = resized.size

        resized.thumbnail((640, 480))

        panel.photo = resized
        new_image = ImageTk.PhotoImage(resized)
        panel.configure(image = new_image)
        panel.image = new_image

        win.destroy()

    win = tk.Toplevel()
    win.wm_title("Re-Aspect")

    width_input = tk.Entry(win)
    width_input.insert(tk.END, 'width component')
    width_input.grid(row = 0, column = 0)

    label = tk.Label(win, text = ":")
    label.grid (row = 0, column = 1)
    
    height_input = tk.Entry(win)
    height_input.insert(tk.END, 'height component')
    height_input.grid(row = 0, column = 2)

    b = tk.Button(win, text="Okay", command = helper)
    b.grid(row=1, column=1)


def gui_ring_blend (panel):
    def helper():
        to_blend = Image.open(blend_path_input.get())
        
        blended = alternateRings(panel.photo.resize (panel.size), to_blend)

        blended.thumbnail((640, 480))

        panel.photo = blended
        new_image = ImageTk.PhotoImage(blended)
        panel.configure(image = new_image)
        panel.image = new_image

        win.destroy()

    win = tk.Toplevel()
    win.wm_title("Ring Blend")

    blend_path_input = tk.Entry(win)
    blend_path_input.insert(tk.END, 'path to image')
    blend_path_input.grid(row = 0, column = 0)

    b = tk.Button(win, text="Okay", command = helper)
    b.grid(row=1, column=0)
    

def gui_shuffle (panel):
    def helper2():
        new_image = shuffle.shuffle2x2(panel.photo)

        panel.photo = new_image
        new_image = ImageTk.PhotoImage(new_image)
        panel.configure(image = new_image)
        panel.image = new_image

        win.destroy()
    
    def helper3():
        new_image = shuffle.shuffle3x3(panel.photo)

        panel.photo = new_image
        new_image = ImageTk.PhotoImage(new_image)
        panel.configure(image = new_image)
        panel.image = new_image

        win.destroy()

    win = tk.Toplevel()
    win.wm_title("Shuffle")


    b2 = tk.Button(win, text="2x2", command = helper2)
    b2.grid(row=0, column=0)

    b2 = tk.Button(win, text="3x3", command = helper3)
    b2.grid(row=0, column=1)
    


def load_image_from_file_path (panel, path):
    im = Image.open(path)
    panel.size = im.size
    panel.photo = im

    im.thumbnail((640, 480))

    img = ImageTk.PhotoImage(im)
    panel.configure(image = img)
    panel.image = img

def save_image(panel, path):
    to_save = panel.photo.resize(panel.size)
    to_save.save(path)
    


def main():
    root = tk.Tk()
    panel = tk.Label(root)
    root.geometry("665x665")
    root.resizable(0, 0)
    
    load_image_from_file_path (panel, './images/flowers-wall.jpg')

    panel.grid(columnspan = 10, ipadx = (665 - panel.photo.size[0]), ipady = (505 - panel.photo.size[1]))

    ring_blend_button = tk.Button(root, width = 15, text="Ring Blend", command= lambda : gui_ring_blend (panel))
    ring_blend_button.grid(row = 1, column = 0, ipadx = 5, ipady = 5)

    aspect_ratio_adjust_button = tk.Button(root, width = 15, text="Adjust aspect ratio", command = lambda : gui_adjust_aspect_ratio(panel))
    aspect_ratio_adjust_button.grid(row = 1, column = 1, ipadx = 5, ipady = 5)

    shuffle_button = tk.Button(root, width = 15, text="Shuffle image", command= lambda : gui_shuffle(panel))
    shuffle_button.grid(row = 2, column = 0, ipadx = 5, ipady = 5)

    color_reverse_button = tk.Button(root, width = 15, text="Reverse Colors", command= lambda:callback(panel))
    color_reverse_button.grid(row = 2, column = 1, ipadx = 5, ipady = 5)

    entry = tk.Entry(root)
    entry.insert(tk.END, 'image name')
    entry.grid(row = 2, column = 4, columnspan = 2, ipadx = 120)

    save_button = tk.Button(root, text="Save", command= lambda : save_image (panel, entry.get()))
    save_button.grid(row = 1, column = 4, ipadx = 5, ipady = 5)

    load_button = tk.Button(root, text="Load", command = lambda : load_image_from_file_path(panel, entry.get()))
    load_button.grid(row = 1, column = 5, ipadx = 5, ipady = 5)


    root.mainloop()

if __name__ == "__main__":
    main()